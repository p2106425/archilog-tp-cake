<aside>
    <div id="sidebar" class="nav-collapse ">
        <ul class="sidebar-menu" id="nav-accordion">
            <p class="centered">
                <a href="#">
                    <img src="/img/users/<?= ($_SESSION['user']['id']??1) ?>.jpg" class="img-circle" width="80">
                </a>
            </p>
            <h5 class="centered">
                <?= $_SESSION['user']['name']??"" ?>
            </h5>
            <li class="mt">
                <?php echo $this->Html->link('<i class="fa fa-dashboard"></i> <span>Tableau de bord</span>', ['controller' => 'Pages', 'action' => 'index'], ['escapeTitle' => false]);?>
            </li>
            <li class="mt">
                <?php echo $this->Html->link('<i class="fa fa-list"></i> <span>Les tickets</span>', ['controller' => 'Tickets', 'action' => 'listing'], ['escapeTitle' => false]);?>
            </li>
        </ul>
    </div>
</aside>